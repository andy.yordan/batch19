var flag = 0;

console.log("LOOPING PERTAMA")
while(flag < 20) { 
  flag += 2; 
  console.log(flag + " - I love coding");   
}

console.log("LOOPING KEDUA")
while(flag >= 2) { 
  console.log(flag + " -  I will become a mobile developer"); 
  flag -= 2; 
}