import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const AboutProject = ({title, desc, icon, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.wrapper}>
                <Icon style={styles.icon} name={icon} size={50} />
                <View style={styles.label}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.desc}>{desc}</Text>
                </View>
                <Icon style={styles.icon} name="chevron-forward" size={50} />
            </View>
            <View style={{borderWidth:0.5, borderColor:'white'}} />            
        </TouchableOpacity>
    )
}

export default AboutProject

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        marginBottom: 10
    },
    icon: {
        color: 'white'
    },
    label: {
        flex: 1,
        paddingLeft: 12
    },
    title: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700'
    },
    desc: {
        color: 'white',
        fontSize: 12,
        fontWeight: '300',
        marginTop: 3
    },
})
