import React from 'react'
import { StyleSheet, Image, View } from 'react-native'

const Avatar = ({source, size}) => {
    return (
        <View style={styles.avatarWrapper(size)}>
            <Image source={source} size={size} resizeMode='contain' style={{flex:1, width:size}} />
        </View>
    )
}

export default Avatar

const styles = StyleSheet.create({
    avatarWrapper: (size) => ({        
        width: size+10,
        height: size+10,
        borderWidth: 1,
        borderColor: '#999999',
        borderRadius: (size+10)/2,
        alignItems: 'center',
        justifyContent: 'center'
    }),  
})
