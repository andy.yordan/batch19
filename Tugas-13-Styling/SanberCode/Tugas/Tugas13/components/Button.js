import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

const Button = ({title, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.label}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#3EC6FF',
        borderRadius: 10,
        paddingVertical: 12
    },
    label: {
        fontSize: 20,
        fontWeight: '700',
        textAlign: 'center',
        color: '#003366'
    }
})
