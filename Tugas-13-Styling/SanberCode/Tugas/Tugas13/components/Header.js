import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Gap from './Gap'
import { AuthContext } from "../../TugasNavigation/context";

const Header = ({title, icon, isSignOut, onPress}) => {
    const { signOut } = React.useContext(AuthContext);
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <Icon style={styles.back} name={icon} size={50} />
            </TouchableOpacity>
            <Text style={styles.title}>{title}</Text>            
            {isSignOut==="true" ? (
                <TouchableOpacity onPress={() => signOut()} >
                    <Icon style={styles.back} name="exit" size={50} />
                </TouchableOpacity>
            ) : (
                <Gap width={50} />
            )}
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        height: 90,
        backgroundColor: 'white',
        borderBottomStartRadius: 40,
        borderBottomEndRadius: 40,
        elevation: 3,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        flexDirection: 'row'
    },
    title: {
        fontSize: 36,
        fontWeight: '700',
        color: '#003366',
    },
    back: {
        color: '#003366'
    }
})
