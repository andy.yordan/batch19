import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

const Category = ({title, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.label}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Category

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#EFEFEF',
        borderRadius: 40,
        width: 110,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'center',
        color: '#003366'
    }
})
