import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native'
import Button from './components/Button'

const LoginScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={styles.hero}>
                <Image source={require('./images/hero.png')} style={styles.heroImage} />
            </View>
            <View style={styles.loginContainer}>
                <Text style={styles.title}>Login</Text>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Username:</Text>
                    <TextInput style={styles.input} />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Password:</Text>
                    <TextInput style={styles.input} />
                </View>
                <View style={styles.buttonContainer}>
                    <Button title='Sign In' onPress={() => navigation.navigate('AboutScreen')} />
                </View>
                <View style={styles.registerContainer}>
                    <Text style={styles.label}>Don't have an account? </Text>
                    <TouchableOpacity onPress={() => alert('Available soon')} >
                        <Text style={styles.register}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#003366',
        color: 'white',
        flex: 1
    },
    hero: {
        height: 270,
        backgroundColor: 'white',
        borderBottomStartRadius: 40,
        borderBottomEndRadius: 40,
        elevation: 3,
        alignItems: 'center',
        paddingTop: 10
    },
    heroImage: {
        paddingHorizontal: 18,        
        height: 250,
        width: '100%'        
    },
    loginContainer: {
        paddingHorizontal: 40,
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 20,
        alignSelf: 'stretch'
    },
    title: {
        fontSize: 32,
        fontWeight: '600',
        color: 'white',
    },
    inputContainer: {
        marginTop: 15,
        alignSelf: 'stretch'
    },
    label: {
        fontSize: 18,
        color: 'white',
        marginBottom: 8,
        fontWeight: '400'
    },
    input: {
        borderWidth: 1, 
        borderColor: '#666666', 
        borderRadius: 10, 
        backgroundColor: 'white', 
        padding: 10
    },
    buttonContainer: {
        marginTop: 30,
        alignSelf: 'stretch'
    },
    registerContainer: {
        marginTop: 30,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    register: {
        fontSize: 18,
        color: 'white',
        marginBottom: 8,
        fontWeight: '700'
    },
})
