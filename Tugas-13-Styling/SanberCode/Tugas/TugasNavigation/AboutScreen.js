import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import AboutProject from '../Tugas13/components/AboutProject'
import Avatar from '../Tugas13/components/Avatar'
import Header from '../Tugas13/components/Header'

const AboutScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Header title='About Me' icon="arrow-back-circle" isSignOut="false" onPress={() => navigation.goBack()} />
            <View style={styles.profile}>
                <Avatar source={require('../Tugas13/images/tony-stark.png')} size={140} />
                <Text style={styles.name}>Yordan Andy</Text>
                <Text style={styles.skill}>Mobile Developer</Text>
                <View style={styles.socmed}>
                    <TouchableOpacity onPress={() => alert('Available soon')}>
                        <Icon style={styles.socmedIcon} name="logo-instagram" size={40} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => alert('Available soon')}>
                        <Icon style={styles.socmedIcon} name="logo-facebook" size={40} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => alert('Available soon')}>
                        <Icon style={styles.socmedIcon} name="logo-twitter" size={40} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.projects}>
                <AboutProject icon='logo-github' title='Github Projects' desc='Explore my cool projects on Github' onPress={() => alert('Available soon')} />
                <AboutProject icon='md-logo-firebase' title='Others Projects' desc='Explore my cool projects on Firebase' onPress={() => alert('Available soon')} />
                <AboutProject icon='card' title='Donate Us' desc='Please support us develop projects' onPress={() => alert('Available soon')} />
            </View>            
        </View>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#003366',
        color: 'white',
        flex: 1,
    },
    profile: {
        marginTop: 12,
        alignItems: 'center',
        paddingHorizontal: 60
    },
    name: {
        fontSize: 28,
        fontWeight: '600',
        color: 'white',
        marginBottom: 5
    },
    skill: {
        fontSize: 16,
        fontWeight: '300',
        color: 'white'
    },
    socmed: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch',
        marginTop: 15
    },
    socmedIcon: {
        color: 'white'
    },
    projects:{
        paddingTop: 20,
        paddingHorizontal: 40
    }
})
