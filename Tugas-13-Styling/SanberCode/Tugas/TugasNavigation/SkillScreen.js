import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import Avatar from '../Tugas13/components/Avatar'
import Button from '../Tugas13/components/Button'
import Category from '../Tugas13/components/Category'
import Header from '../Tugas13/components/Header'
import SkillBox from '../Tugas14/SkillBox'
import data from '../Tugas14/skillData.json'

const SkillScreen = ({ navigation }) => {
    return (
        <ScrollView style={styles.container}>
            <Header title='Skill' icon="menu" isSignOut="true" onPress={() => navigation.toggleDrawer()} />
            <View style={styles.profile}>
                <Avatar source={require('../Tugas13/images/steve-rogers.png')} size={80} />
                <View style={styles.user}>
                    <Text style={styles.name}>Steve Rogers</Text>
                    <Text style={styles.skill}>Fullstack Web Developer</Text>
                </View>
            </View>
            <View style={styles.body}>
                <View style={styles.category}>
                    <Category title='Framework' onPress={() => alert('Available soon')} />
                    <Category title='Programming' onPress={() => alert('Available soon')} />
                    <Category title='Technology' onPress={() => alert('Available soon')} />
                </View>                
                <View style={styles.skillWrapper}>                    
                    {(data.items).map((skill, index) =>
                        <SkillBox key={index} skill={skill}/>
                    )}
                </View>
                <Button title='Hire Me' onPress={() => alert('Thank you')} />
            </View>
        </ScrollView>
    )
}

export default SkillScreen

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#003366',
        color: 'white',
        flex: 1,
    },
    body: {
        paddingHorizontal: 20,
        marginBottom: 30
    },
    profile: {
        flexDirection: 'row',
        alignItems: 'center',        
        flex: 1,
        marginTop: 20,        
        alignSelf: 'center',
        paddingHorizontal: 20,
    },
    user: {
        marginLeft: 20,
        flex: 1
    },
    name: {
        fontSize: 28,
        fontWeight: '600',
        color: 'white',
        marginBottom: 2
    },
    skill: {
        fontSize: 14,
        fontWeight: '300',
        color: 'white'
    },
    category: {
        flexDirection: 'row',
        marginTop: 15,
        justifyContent: 'space-between'
    },
    skillWrapper: {
        marginTop: 20
    }
})
