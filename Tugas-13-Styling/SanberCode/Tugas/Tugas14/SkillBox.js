import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const SkillBox = ({skill}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => alert('Available soon')} >            
                <Icon style={styles.icon} name={skill.iconName} size={80} />
                <View style={styles.label}>
                    <Text style={styles.title}>{skill.skillName}</Text>
                    <Text style={styles.desc}>{skill.categoryName}</Text>
                </View>
                <Text style={styles.percentage}>{skill.percentageProgress}</Text>      
        </TouchableOpacity>
    )
}

export default SkillBox

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: 20,
        padding: 10,
        marginBottom: 20,
        backgroundColor: '#EFEFEF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        color: '#003366'
    },
    label: {
        flex: 1,
        paddingLeft: 8
    },
    title: {
        color: '#003366',
        fontSize: 24,
        fontWeight: '700'
    },
    desc: {
        color: '#003366',
        fontSize: 14,
        fontWeight: '300',
        marginTop: 3
    },
    percentage: {
        color: '#003366',
        fontSize: 36,
        fontWeight: '700',
    },
})
