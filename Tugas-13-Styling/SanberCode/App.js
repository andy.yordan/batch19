import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import Noter from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import TugasNavigation from './Tugas/TugasNavigation'
import Quiz3 from './Tugas/Quiz3'

const Stack = createStackNavigator();

const App = () => {
  return (

    <Quiz3 />

    // <NavigationContainer>
    //   <Stack.Navigator initialRouteName="LoginScreen">
    //     <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerShown: false}} />
    //     <Stack.Screen name="AboutScreen" component={AboutScreen} options={{headerShown: false}} />
    //   </Stack.Navigator>
    // </NavigationContainer>

    // <Noter />

    // <SkillScreen />

    // <TugasNavigation />


  )
}

export default App;