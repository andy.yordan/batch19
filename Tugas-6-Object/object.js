//Soal No.1
console.log("===Soal No.1===");
function arrayToObject(arr) {
    if (typeof arr !== 'undefined' && arr.length > 0) {    
        var now = new Date()
        var thisYear = now.getFullYear()    
        
        for(var i = 0; i <= arr.length-1; i++) {
           
            var firstName = '';
            var lastName = '';
            var gender = '';
            var age = '';
            var objPeople = {};

            firstName = arr[i][0];
            lastName = arr[i][1];
            if(arr[i][2] == 'male' || arr[i][2] == 'female') 
                gender = arr[i][2];
            else 
                gender = 'undefined';
            if(Number(arr[i][3]) < Number(thisYear))
                age = Number(thisYear) - Number(arr[i][3]);
            else   
                age = '"Invalid Birth Year"';

            objPeople.firstName = firstName;
            objPeople.lastName = lastName;
            objPeople.gender = gender;
            objPeople.age = age;

            // console.log(objPeople);
            console.log(i+1 + '. ' + firstName + ' ' + lastName + ': {');
            console.log('    firstName: "' + objPeople.firstName + '",');
            console.log('    lastName: "' + objPeople.lastName + '",');
            console.log('    gender: "' + objPeople.gender + '",');
            console.log('    age: ' + objPeople.age);            
            console.log('}');            

        }

    } else {
        console.log('Array tidak boleh kosong');
    }
    console.log("");
}

arrayToObject([]); 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);

console.log("===Soal No.2===");
function shoppingTime(memberId, money) {
    var result = '';
    if (typeof memberId !== 'undefined' && memberId.length > 0) {           
        if(Number(money) < 50000){
            result = 'Mohon maaf, uang tidak cukup';
        } else {
            var objShopping = {};
            var listPurchased = [];
            var changeMoney = 0;
            var sepatuStacattu = 1500000;
            var bajuZoro = 500000;
            var bajuHN = 250000;
            var sweaterUniklooh = 175000;
            var casingHandphone = 50000;
            var saldoMoney = money;
            
            if(saldoMoney>=sepatuStacattu){
                listPurchased.push('Sepatu Stacattu');
                saldoMoney -= sepatuStacattu;
            }
            if(saldoMoney>=bajuZoro){
                listPurchased.push('Baju Zoro');
                saldoMoney -= bajuZoro;
            }
            if(saldoMoney>=bajuHN){
                listPurchased.push('Baju H&N');
                saldoMoney -= bajuHN;
            }
            if(saldoMoney>=sweaterUniklooh){
                listPurchased.push('Sweater Uniklooh');
                saldoMoney -= sweaterUniklooh;
            }
            if(saldoMoney>=casingHandphone){
                listPurchased.push('Casing Handphone');
                saldoMoney -= casingHandphone;
            }                
            
            changeMoney = saldoMoney;
            
            objShopping.memberId = memberId;
            objShopping.money = money;
            objShopping.listPurchased = listPurchased;
            objShopping.changeMoney = changeMoney;

            result = JSON.stringify(objShopping);
        }
    } else {
        result = 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }
    result += '\n';
    return result;
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("===Soal No.3===");
function naikAngkot(arrPenumpang) {
    var arrResult = [];    

    if (typeof arrPenumpang !== 'undefined' && arrPenumpang.length > 0) { 
        var rute = ['A', 'B', 'C', 'D', 'E', 'F'];           
        for(var i = 0; i <= arrPenumpang.length-1; i++) {
            var objPenumpang = {};
            objPenumpang.penumpang = arrPenumpang[i][0];
            objPenumpang.naikDari = arrPenumpang[i][1];
            objPenumpang.tujuan = arrPenumpang[i][2];

            var totalBayar = 0;
            var mulaiHitung = false;
            for(var j = 0; j < rute.length; j++) {
                if(mulaiHitung) totalBayar += 2000;
                if(rute[j] == objPenumpang.naikDari) mulaiHitung = true;                
                if(rute[j] == objPenumpang.tujuan) mulaiHitung = false;                
            }
            objPenumpang.bayar = totalBayar;
            arrResult.push(objPenumpang);
        }
    } else {
        arrResult = [];
    }    
    return arrResult;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'], ['Joko', 'A', 'F'], ['Dewo', 'C', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  //   { penumpang: 'Joko', naikDari: 'A', tujuan: 'F', bayar: 10000 } ]
  //   { penumpang: 'Dewo', naikDari: 'C', tujuan: 'B', bayar: 6000 } ] bayar sampai tujuan akhir di F
console.log(naikAngkot([])); //[]