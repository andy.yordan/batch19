//Soal If-Else
var nama = "John"
var peran = "Penyihir"

console.log("===Tugas If-Else===")
if (nama == "" && peran == "") {
    console.log("Nama harus diisi!")
} else if (peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
} else {
    if (peran.toLowerCase() == "penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (peran.toLowerCase() == "guard") {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (peran.toLowerCase() == "werewolf") {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!")
    } else {
        console.log("Halo " + nama + ", Pilihan peran hanya ada Penyihir, Guard, dan Werewolf.")
    }
}
console.log(' ');

//Soal Switch-Case
var tanggal = 17;
var bulan = 8;
var tahun = 1945;

var maxTanggal = 31
var isKabisat = false
var namaBulan = ""

console.log("===Tugas Switch-Case===")
if (tahun < 1900 || tahun > 2200) {
    console.log("Masukan tahun antara 1900 s.d 2200")
} else {
    if (bulan < 1 || bulan > 12) {
        console.log("Masukan bulan antara 1 s.d 12")
    } else {
        if (tanggal < 1) {
            console.log("Tanggal tidak boleh kurang dari 1")
        } else {
            if (((tahun % 4) == 0 && (tahun % 100) != 0) ||  (tahun % 400) == 0) isKabisat = true
            
            switch(bulan){
                case 2 : {
                    if (((tahun % 4 == 0) && !(tahun % 100 == 0)) || (tahun % 400 == 0))
                        maxTanggal = 29;
                    else
                        maxTanggal = 28;
                    break;
                }
                case 4 : case 6: case 9 : case 11 : 
                    maxTanggal = 30;
                    break;
                default : maxTanggal = 31 
            }

            if (tanggal > maxTanggal) 
                console.log("Tanggal tidak valid")
            else {
                switch(bulan){
                    case 1 : { namaBulan = "Januari" ; break; }
                    case 2 : { namaBulan = "Februari" ; break; }
                    case 3 : { namaBulan = "Maret" ; break; }
                    case 4 : { namaBulan = "April" ; break; }
                    case 5 : { namaBulan = "Mei" ; break; }
                    case 6 : { namaBulan = "Juni" ; break; }
                    case 7 : { namaBulan = "Juli" ; break; }
                    case 8 : { namaBulan = "Agustus" ; break; }
                    case 9 : { namaBulan = "September" ; break; }
                    case 10 : { namaBulan = "Oktober" ; break; }
                    case 11 : { namaBulan = "November" ; break; }
                    case 12 : { namaBulan = "Desember" ; break; }
                    default : namaBulan = ""
                }

                console.log(tanggal + " " + namaBulan + " " + tahun)
            }
        }
        
    }
}























