let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const startReadBooks = (time, books, i) => {
    if(i < books.length) {
        readBooksPromise(time, books[i])
            .then((timeLeft) => {
                i += 1;
                startReadBooks(timeLeft, books, i);            
            })
            .catch((timeLeft) => {
                if(timeLeft < 0) timeLeft = 0;                
                console.log(`Sisa waktu ${timeLeft}`);
            })
    }
}

startReadBooks(10000, books, 0);
// startReadBooks(5000, books, 0);
// startReadBooks(2000, books, 0);