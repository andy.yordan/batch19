let readBooks = require('./callback.js')

let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const startReadBooks = (time, books, i) => {
    if(i < books.length) {
        readBooks(time, books[i], (timeLeft) => { 
            if(timeLeft > 0) {
                i += 1;
                startReadBooks(timeLeft, books, i);
            }
        })
    }
}

startReadBooks(10000, books, 0);
// startReadBooks(5000, books, 0);
// startReadBooks(2000, books, 0);